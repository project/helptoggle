<?php

/**
 * Generate Help Toggle Settings form
 */
function helptoggle_settings(&$form_state) {

  $form['javascript_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Javascript options'),
    '#collapsible' => TRUE,
  );

  $form['javascript_options']['js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use javascript'),
    '#default_value' => variable_get('helptoggle_js', 1),
    '#description' => t('Use javascript for hiding and showing help texts.'),
  );

  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );


  $form['page_vis_settings']['pages_include'] = array(
    '#type' => 'textarea',
    '#title' => t('Include pages'),
    '#default_value' => variable_get('helptoggle_include_pages', ''),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. If left empty Help Toggle is used on every page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  $form['page_vis_settings']['pages_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude pages'),
    '#default_value' => variable_get('helptoggle_exclude_pages',''),
   '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. Excluding overrides including.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );

  $form['display_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global display options'),
    '#collapsible' => TRUE,
  );

  $form['display_options']['disable_help'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable help'),
    '#default_value' => variable_get('helptoggle_disable_help', 0),
    '#description' => t('Don\'t load help texts. If you select this help cannot be toggled on individual pages.'),
  );

  $form['display_options']['disable_descriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable descriptions'),
    '#default_value' => variable_get('helptoggle_disable_descriptions', 0),
    '#description' => t('Don\'t load form element descriptions. If you select this descriptions cannot be toggled on individual pages.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

function helptoggle_settings_submit($form, &$form_state) {
  variable_set('helptoggle_js', $form['javascript_options']['js']['#value']);
  variable_set('helptoggle_include_pages', $form['page_vis_settings']['pages_include']['#value']);
  variable_set('helptoggle_exclude_pages', $form['page_vis_settings']['pages_exclude']['#value']);
  variable_set('helptoggle_disable_help', $form['display_options']['disable_help']['#value']);
  variable_set('helptoggle_disable_descriptions', $form['display_options']['disable_descriptions']['#value']);
  drupal_set_message(t('Configuration saved.'));
}
